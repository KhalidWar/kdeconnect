import 'package:flutter/material.dart';

class Tabs {
  Tabs({this.title, this.widget});

  String title;
  Widget widget;
  Function function;
}
