import 'package:flutter/material.dart';

// Send Files Tab
const kSendFilesIDText = 'Send Files';

// Slideshow Remote Tab
const kSlideshowRemoteIDText = 'Slideshow Remote';

// Media Control Tab
const kMediaControlIDText = 'Multimedia Control';

// Remote Input Tab
const kRemoteInputIDText = 'Remote Input';

const kFABKeyboardUpIcon = Icon(Icons.keyboard, size: 43);
const kFABKeyboardDownIcon = Icon(Icons.keyboard_hide, size: 43);

// Run Command Tab
const kRunCommandIDText = 'Run Command';
const kCommandsLabelTextStyle =
    TextStyle(fontSize: 25, fontWeight: FontWeight.w500);

// Drawer
const kDrawerItemsTextStyle =
    TextStyle(fontSize: 20, fontWeight: FontWeight.w500);
